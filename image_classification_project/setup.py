from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='In this project, we will demonstrate image classification workflow using deep learning algorithms.',
    author='Grupa 4',
    license='',
)
